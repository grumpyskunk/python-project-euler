from functools import reduce


class EulerCommon:
    """
    Class provides helper methods for Euler exercises.
    None of these methods are self-aware and should not require state management
    """

    @staticmethod
    def is_divisible(numerator: int, denominator: int) -> bool:
        """
        Evaluates if a given numerator is cleanly divisible by the given denominator

        :param numerator:
        :param denominator:
        :return: Boolean result of evaluation
        """
        return not numerator % denominator

    @staticmethod
    def is_leap_year(year: int) -> bool:
        """
         With Gregorian calendar a leap year is determined in the following order or priority
        Divisible by 400: Yes
        Divisible by 100: No
        Divisible by 4:   Yes
        Otherwise:        No

        :param year: Year to evaluate for
        :return:
        """

        leap_year = False
        if EulerCommon.is_divisible(year, 400):
            leap_year = True
        elif EulerCommon.is_divisible(year, 100):
            leap_year = False
        elif EulerCommon.is_divisible(year, 4):
            leap_year = True

        return leap_year

    @staticmethod
    def get_binomial_coefficient(pool: int, select: int) -> int:
        """
        Retrieves the number of possibilities for a given selection within a defined quantity

        e.g.
        Imagine you have 5 elements {a, b, c, d, f}.
        To find out how many different subsets of 2 elements it has, look at the binomial coefficient
           5!
        -------   = 10
        2!(5-2)!

        :param pool: Represents total number of options available to choose from
        :param select: Represents the total number of options being selected
        :return:
        """
        factorial_n = EulerCommon.get_factorial(pool)
        factorial_k = EulerCommon.get_factorial(select) * EulerCommon.get_factorial(pool - select)

        if not factorial_n or not factorial_k:
            return 0

        return factorial_n // factorial_k

    @staticmethod
    def get_central_binomial_coefficient(number: int) -> int:
        """
        Retrieves the central number to a pascals triangle
        See: https://www.robertdickau.com/manhattan.html

        :param number:
        :return:
        """
        factorial_2_n = EulerCommon.get_factorial(2 * number)
        factorial_n = EulerCommon.get_factorial(number)

        return factorial_2_n // factorial_n ** 2

    @staticmethod
    def get_factorial(number: int) -> int:
        """
        Retrieves the factorial value of a given number
        e.g. !5 = 5 x 4 x 3 x 2 x 1

        :param number: Number to retrieve factorial for
        :return:
        """
        # If we're already as low as we can go
        if number == 1:
            return number

        # Our sequence will begin one step short of our number
        sequence = number - 1

        # We can stop at two since any number times one is itself
        while sequence > 1:
            number = number * sequence

            sequence -= 1

        return number

    @staticmethod
    def get_factors(number: int) -> set:
        """
        Determines all factors for a given value

        Source: https://stackoverflow.com/a/6800214
        Using list comprehension we are returning the value of i and the value of the given number divided
        to the nearest whole integer as a list if i is cleanly divisible by our number.

        The reduce is adding those lists of two factors together into a single list which is then made
        unique by returning as a set.

        Since a number is not divisible by a number larger than its half (other than itself)
        we iterate to the square root of our number.

        Example; factors for 28 return the following lists on each iteration...
        - [1, 28]
        - [2, 14]
        - [4, 7]

        See also: https://www.programiz.com/python-programming/list-comprehension

        :param number: Number to determine factors for
        :return: Set containing all factors to a given number
        """
        return set(
            reduce(
                list.__add__,
                ([i, number // i] for i in range(1, int(number ** 0.5) + 1) if EulerCommon.is_divisible(number, i))
            )
        )

    @staticmethod
    def get_list_product(to_product: list):
        """
        Uses a lambda to reduce a list to the product of its values

        :param to_product:
        :return:
        """
        if not len(to_product):
            return 0

        return reduce((lambda x, y: x * y), to_product)

    @staticmethod
    def get_list_sum(to_sum: list):
        """
        Uses a lambda to reduce a list to the sum of its values

        :param to_sum:
        :return:
        """
        if not len(to_sum):
            return 0

        return reduce((lambda x, y: x + y), to_sum)

    @staticmethod
    def get_permutations(to_permutate: list):
        """
        Retrieves the permutations of a given list
        Adaptation from: https://www.geeksforgeeks.org/generate-all-the-permutation-of-a-list-in-python/

        :param to_permutate:
        :return:
        """
        size = len(to_permutate)

        # If we have nothing to work with...
        if not size:
            return []

        # If there is only one element...
        if size == 1:
            return [to_permutate]

        # Initialize our result and iterate for size
        result = []
        for index in range(size):
            # Retrieve for this value
            value = to_permutate[index]

            # Retrieve remaining list values using step
            remaining = to_permutate[:index] + to_permutate[index + 1:]

            # Recursively append to this value for each remaining
            for next_val in EulerCommon.get_permutations(remaining):
                # Append this value as a list with the result of the recursive return
                result.append([value] + next_val)

        return result

    @staticmethod
    def get_prime_factors(number: int) -> set:
        """
        Determines the prime factors for a given value

        :param number:
        :return: Set containing prime factors to given number
        """
        result = set()

        # While the number is even, continue to reduce and add 2 as a valid factor
        while EulerCommon.is_divisible(number, 2):
            result.add(2)
            number = number // 2

        # Similarly to detecting primes, we do not need to exceed the square root
        # We can also start at 3 and step by 2 as we only need to test odds now
        for test in range(3, int(number ** 0.5) + 1, 2):
            # While this number remains divisible by our test, continue to reduce
            while EulerCommon.is_divisible(number, test):
                result.add(test)
                number = number // test

            if number == 1:
                break

        # If the remaining number is greater than two, then it is also prime
        if number > 2:
            result.add(number)

        # Return our determined list
        return result

    @staticmethod
    def get_primes(goal: int) -> list:
        """
        We will apply the sieve algorithm since multiplication is computationally much easier

        Basically starting from two to our goal we will compute and negate the prime possible
        multiples of every value. This cascading process will rapidly reduce the possible
        computations within our range.

        :param goal:
        :return:
        """
        primes = []
        potentials = range(2, goal)
        sieve = {index: True for index in potentials}

        for number, prime_possible in sieve.items():
            if not prime_possible:
                continue

            # Starting at the next multiple, step forward to our goal value at iterations of our new prime
            for cancel in range(number + number, goal, number):
                sieve[cancel] = False

            # Add this value to our prime list
            primes.append(number)

        return primes

    @staticmethod
    def get_weekday(month: int, day: int, year: int) -> int:
        """
        Retrieves the weekday for a given month, day, year using the key value method

        :param month:
        :param day:
        :param year:
        :return:
        """
        month_map = {1: 1, 2: 4, 3: 4, 4: 0, 5: 2, 6: 5, 7: 0, 8: 3, 9: 6, 10: 1, 11: 4, 12: 6}
        year_map = {17: 4, 18: 2, 19: 0, 20: 6}
        """
        With the key value method the remainder represents the weekday
        In this case remainder 1 is Sunday and remainder zero is Saturday
        This map allows us to return the value consistent with most common
        calendars which represent Sunday as the first day of the week (index zero)
        """
        weekday_map = {1: 0, 2: 1, 3: 2, 4: 3, 5: 4, 6: 5, 0: 7}

        year_first_two = int(str(year)[:2])
        year_last_two = int(str(year)[2:])
        # Divide the final two digits of the year by four, dropping any remainder
        result = year_last_two // 4
        # Add our day of the month
        result += day
        # Add the value of our month
        result += month_map[month]

        # If we are evaluating for January or February of a leap year, subtract one
        if (month == 1 or month == 2) and EulerCommon.is_leap_year(year):
            result -= 1

        # If our first two digits of our year are outside of our mapped values
        # add/subtract 4 (representing Gregorian calendar 400 year repeat)
        # until they are within range
        if year_first_two > 20:
            while year_first_two > 20:
                year_first_two -= 4
        elif year_first_two < 17:
            while year_first_two < 17:
                year_first_two += 4

        # Add value of our year
        result += year_map[year_first_two]

        # Add the last two digits of our year
        result += year_last_two

        # Weekday will be the remainder of our result / 7
        weekday = int(result % 7)

        return weekday_map[weekday]
