class EulerFileIo:

    def __init__(self) -> None:
        super().__init__()

    @classmethod
    def read_file(cls, path: str) -> str:
        contents = []

        with open(path) as file:
            contents = file.read()

        return contents
