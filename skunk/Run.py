import time
import importlib


def get_demo(option: int):
    """
    Invoke the appropriate euler definition

    :param option:
    :return: int
    """
    try:
        module = importlib.import_module(f'.Euler{option}', package='skunk.eulers')
        euler = getattr(module, f'Euler{option}')
    except ImportError as error:
        print(f'Unable to load Euler{option} demonstration. Confirm solution file exists.')
        exit(1)

    return euler


if __name__ == '__main__':
    """
    Runs a given Euler script
    """

    response = input('\nSelect a Euler demonstration to execute: ')

    # If the response is not a valid numeric input...
    if not response.isdigit():
        print(f'Invalid input provided: {response}. Number expected.')
        exit(1)

    euler = get_demo(int(response))

    # Execute the runner
    timestamp = time.time()

    solution = euler.run()

    elapsed = time.time() - timestamp

    print(f'\tSolution: {solution}')
    print(f'\tElapsed: {elapsed:1.3f} seconds')
