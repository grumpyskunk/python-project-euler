from skunk.EulerCommon import EulerCommon


class Euler5:
    """
    2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.

    What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
    """

    @staticmethod
    def run():
        print(Euler5.__doc__)

        solution = None

        # Start at 2520 since no number smaller is wholly divisible by 1-10, and 20
        current = 2520
        """
        Since we know the number must be divisible by 20 and 1-10, we can iterate by 2520
        as well as reduce the number of values we need to check within the range of 1-20.
        
        Working backward we will check divisibility for the values 19-11
        """
        denominators = [19, 18, 17, 16, 15, 14, 13, 12, 11]

        while solution is None:
            # Assume the current number is the solution we need
            solution = current

            # Begin testing for divisibility
            for denominator in denominators:
                # If any number is not divisible...
                if not EulerCommon.is_divisible(current, denominator):
                    # We have not found a solution
                    solution = None
                    break

            # Iterate
            current += 2520

        return solution
