from skunk.EulerCommon import EulerCommon


class Euler10:
    """
        The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

        Find the sum of all the primes below two million.
        """

    @staticmethod
    def run():
        print(Euler10.__doc__)

        goal = 2000000
        primes = EulerCommon.get_primes(goal)

        # Use reduce to apply multiplication on all multiplicands
        return EulerCommon.get_list_sum(primes)
