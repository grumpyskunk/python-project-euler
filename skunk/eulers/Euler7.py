from skunk.EulerCommon import EulerCommon


class Euler7:
    """
    By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.

    What is the 10,001st prime number?
    """

    @staticmethod
    def run():
        print(Euler7.__doc__)

        # Retrieve all prime numbers up to 500,000
        primes = EulerCommon.get_primes(500_000)
        # Return the 10,001st index
        return primes[10_000]

        """
        # Original solution - it works by creating by sieve is _much_ faster
        
        while length < 10001:
            # Until the next prime is identified...
            while True:
                # Add two to the last prime value so we only evaluate for odd numbers
                is_prime += 2
                # Assume this value is prime
                identified = True

                for prime in primes:
                    # If this value is divisible by a known prime
                    if EulerCommon.is_divisible(is_prime, prime):
                        # Then it itself is not identified as prime
                        identified = False

                        break

                # If we've identified a prime, add it to our list
                if identified:
                    primes.append(is_prime)
                    length += 1

                    break

        return primes.pop()
        """
