class Euler6:
    """
    The sum of the squares of the first ten natural numbers is,
    1^2 + 2^2 + ... +10^2 = 385

    The square of the sum of the first ten natural numbers is,
    (1 + 2 + ... + 10)^2 = 55^2 = 3025

    Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is
    3025 − 385 = 2640

    Find the difference between the sum of the squares of the first one hundred natural numbers
    and the square of their sum.
    """

    @staticmethod
    def run():
        print(Euler6.__doc__)

        square_sum = 0
        natural_sum = 0
        for number in range(1, 101):
            natural_sum += number
            square_sum += (number ** 2)

        return natural_sum ** 2 - square_sum
