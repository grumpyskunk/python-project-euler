from functools import reduce
from skunk.EulerCommon import EulerCommon


class Euler16:
    """
    2^15 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.

    What is the sum of the digits of the number 2^1000?
    """

    @staticmethod
    def run():
        print(Euler16.__doc__)

        # Create a list using the value of 2^1000 as a string then reduce again using a lambda to create the sum
        return EulerCommon.get_list_sum(
            reduce(list.__add__, ([int(n)] for n in str(2 ** 1000)))
        )
