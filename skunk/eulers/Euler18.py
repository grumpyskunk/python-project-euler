from skunk.EulerCommon import EulerCommon


class Euler18:
    """
    By starting at the top of the triangle below and moving to adjacent numbers on the row below,
     the maximum total from top to bottom is 23.

        3
       7 4
      2 4 6
     8 5 9 3

    That is, 3 + 7 + 4 + 9 = 23.

    Find the maximum total from top to bottom of the triangle below:

    NOTE: As there are only 16384 routes, it is possible to solve this problem by trying every route.
    However, Problem 67, is the same challenge with a triangle containing one-hundred rows;
    it cannot be solved by brute force, and requires a clever method! ;o)
    """

    @staticmethod
    def run():
        print(Euler18.__doc__)

        rows = """75
95 64
17 47 82
18 35 87 10
20 04 82 47 65
19 01 23 75 03 34
88 02 77 73 07 63 67
99 65 04 28 06 16 70 92
41 41 26 56 83 40 80 70 33
41 48 72 33 47 32 37 16 94 29
53 71 44 65 25 43 91 52 97 51 14
70 11 33 28 77 73 17 78 39 68 17 57
91 71 52 38 17 14 91 43 58 50 27 29 48
63 66 04 68 89 53 67 30 73 16 69 87 40 31
04 62 98 27 23 09 70 98 73 93 38 53 60 04 23""".split('\n')

        # Formalize our numbers triangle and cast the integers for each row
        triangle = [[int(numbers) for numbers in row.split()] for row in rows]

        # To find the maximum potential sum we have to work from the bottom up, so reverse the triangle
        triangle.reverse()

        solution = 0
        has_lower_row = False
        rolling_row = []

        # Iterate our triangle row by row
        for row in triangle:
            # We will skip the row since there is nothing to increase from
            if not has_lower_row:
                # Set the last row as our rolling row
                rolling_row = row

                # Here forward there will always be a rolling row
                has_lower_row = True
                continue

            # Initialize our sum row to be calculated from the current + the rolling
            sum_row = []
            for index in range(len(row)):
                # Evaluate max of left and right adjacent sums
                l_adjacent = row[index] + rolling_row[index]
                r_adjacent = row[index] + rolling_row[index + 1]
                this_max = max(l_adjacent, r_adjacent)

                sum_row.append(this_max)

            # Set our rolling row as our compiled sum row
            rolling_row = sum_row

        return EulerCommon.get_list_sum(rolling_row)
