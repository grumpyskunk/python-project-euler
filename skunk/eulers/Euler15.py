from skunk.EulerCommon import EulerCommon


class Euler15:
    """
    Starting in the top left corner of a 2×2 grid, and only being able to move to the right and down,
    there are exactly 6 routes to the bottom right corner.

    [ 1]----[ 1]----[ 1] - -[ 1]
     |       |       |       |
     |       |       |       |
    [ 1]----[ 2]----[ 3] - -[ 4]
     |       |       |       |
     |       |       |       |
    [ 1]----[ 3]----[ 6] - -[10]
     |       |       |       |
     |       |       |       |
    [ 1] - -[ 4] - -[10] - -[20]

    How many such routes are there through a 20×20 grid?
    """

    @staticmethod
    def run():
        print(Euler15.__doc__)

        return EulerCommon.get_central_binomial_coefficient(20)
