from skunk.EulerCommon import EulerCommon


class Euler23:
    """
    A perfect number is a number for which the sum of its proper divisors is exactly equal to the number.
    For example, the sum of the proper divisors of 28 would be 1 + 2 + 4 + 7 + 14 = 28,
    which means that 28 is a perfect number.

    A number n is called deficient if the sum of its proper divisors is less than n
    and it is called abundant if this sum exceeds n.

    As 12 is the smallest abundant number, 1 + 2 + 3 + 4 + 6 = 16, the smallest number that can be written as
    the sum of two abundant numbers is 24. By mathematical analysis, it can be shown that all integers
    greater than 28123 can be written as the sum of two abundant numbers. However, this upper limit cannot be
    reduced any further by analysis even though it is known that the greatest number that cannot be expressed as
    the sum of two abundant numbers is less than this limit.

    Find the sum of all the positive integers which cannot be written as the sum of two abundant numbers.
    """

    @staticmethod
    def run():
        print(Euler23.__doc__)

        # We know from the problem doc that 28,123 is the highest possible non-abundant sum value
        limit = 28_123
        # We will use a list to track abundant numbers we identify
        abundant_numbers = []
        # We will use a dictionary to track potentials we identify
        potentials = {index: True for index in range(1, limit)}

        # Start at 12 since we know it is the first abundant number
        for number in range(12, limit):
            # Retrieve the factors for this number, then sort and remove the greatest factor (itself)
            factors = list(EulerCommon.get_factors(number))
            factors.sort()
            factors.pop()

            # Evaluate if the factor sum is greater than the current number
            f_sum = EulerCommon.get_list_sum(factors)
            if f_sum > number:
                # If it is add it to our abundant number list
                abundant_numbers.append(number)
                # Then iterate all abundant numbers to negate their sum from potentials
                for abundant in abundant_numbers:
                    cancel = number + abundant
                    potentials[cancel] = False

        # Using list comprehension get the sum of all numbers within our potentials list that are still valid
        return EulerCommon.get_list_sum([number for number, is_valid in potentials.items() if is_valid])
