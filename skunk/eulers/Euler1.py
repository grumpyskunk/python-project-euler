from skunk.EulerCommon import EulerCommon


class Euler1:
    """
    If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9.
    The sum of these multiples is 23.

    Find the sum of all the multiples of 3 or 5 below 1000.
    """

    @staticmethod
    def run():
        print(Euler1.__doc__)
        total = 0
        # Starting at 3 (since neither 1 or 2 are divisible) for a range of 1000
        for number in range(3, 1001):
            # If divisible by 3 - add it to total and continue
            if EulerCommon.is_divisible(number, 3):
                total += number
                continue

            # If divisible by 5 - add it to total and continue
            if EulerCommon.is_divisible(number, 5):
                total += number
                continue

        return total
