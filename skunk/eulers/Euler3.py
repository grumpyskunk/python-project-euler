from skunk.EulerCommon import EulerCommon


class Euler3:
    """
    The prime factors of 13195 are 5, 7, 13 and 29.

    What is the largest prime factor of the number 600851475143 ?
    """

    @staticmethod
    def run():
        print(Euler3.__doc__)

        factors = EulerCommon.get_prime_factors(600851475143)
        return max(factors)
