from skunk.EulerFileIO import EulerFileIo


class Euler22:
    """
    Using names.txt (right click and 'Save Link/Target As...'), a 46K text file containing over
    five-thousand first names, begin by sorting it into alphabetical order.
    Then working out the alphabetical value for each name, multiply this value by its alphabetical
    position in the list to obtain a name score.

    For example, when the list is sorted into alphabetical order, COLIN, which is worth 3 + 15 + 12 + 9 + 14 = 53,
    is the 938th name in the list. So, COLIN would obtain a score of 938 × 53 = 49714.

    What is the total of all the name scores in the file?
    """

    @staticmethod
    def run():
        print(Euler22.__doc__)

        path = '/home/skunk/PycharmProjects/ProjectEuler/skunk/names.txt'
        io = EulerFileIo()

        # The file is terribly formatted so we need to remove some formatting and split into a list
        names = io.read_file(path).replace('"', '').split(',')
        names.sort()

        solution = 0

        # Enumerate our list and add the name score to the solution total
        for index, name in enumerate(names):
            position = index + 1
            solution += position * Euler22.score_name(name)

        return solution

    @staticmethod
    def score_name(name: str) -> int:
        scale = {'A': 1, 'B': 2, 'C': 3, 'D': 4, 'E': 5, 'F': 6, 'G': 7, 'H': 8, 'I': 9, 'J': 10, 'K': 11, 'L': 12,
                 'M': 13, 'N': 14, 'O': 15, 'P': 16, 'Q': 17, 'R': 18, 'S': 19, 'T': 20, 'U': 21, 'V': 22, 'W': 23,
                 'X': 24, 'Y': 25, 'Z': 26}

        value = 0
        for letter in name.upper():
            value += scale[letter]

        return value
