from skunk.EulerCommon import EulerCommon
from functools import reduce


class Euler24:
    """
    A permutation is an ordered arrangement of objects. For example, 3124 is one possible permutation of the digits
    1, 2, 3 and 4. If all of the permutations are listed numerically or alphabetically, we call it lexicographic order.
    The lexicographic permutations of 0, 1 and 2 are:

    012   021   102   120   201   210

    What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?
    """

    @staticmethod
    def run():
        print(Euler24.__doc__)

        permutations = EulerCommon.get_permutations([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
        permutations.sort()

        # Original - it successfully demonstrated 0,1,2 but I knew I would ultimately need recursion
        # ------
        # Use size to work way down digits list
        # for digit in digits:
        #     ps = [
        #         str(digit) + str(digits[d_index]) + str(digits[z_index])
        #         for d_index in range(size) if digit != digits[d_index]
        #         for z_index in range(size) if digit != digits[z_index] and digits[d_index] != digits[z_index]
        #     ]
        #     permutations.append(ps)

        # Reduce the list for the millionth index to a string
        return reduce((lambda x, y: str(x) + str(y)), permutations[999_999])
