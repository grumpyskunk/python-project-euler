class Euler9:
    """
    A Pythagorean triplet is a set of three natural numbers, a < b < c, for which, a^2 + b^2 = c^2

    For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.

    There exists exactly one Pythagorean triplet for which a + b + c = 1000.
    Find the product abc.
    """

    @staticmethod
    def run():
        print(Euler9.__doc__)

        a_b_c_sum = 1000

        """
        To expedite processing we will define some limits of our A/B sides based on the properties of our
        triplet.

        In the case of an equilateral right triangle, all sides would be an equal size. But with a triplet
        each element is less than the next, so...
        A is limited by (sum / 3) - 1
        B is limited by (sum / 2) - 1 (allocating the remaining 1/2 between A and C)
        C has the remainder value
        """
        a_limit = a_b_c_sum // 3
        b_limit = a_b_c_sum // 2

        for a in range(1, a_limit):
            for b in range(1, b_limit):
                c = a_b_c_sum - a - b

                if a ** 2 + b ** 2 == c ** 2:
                    return a * b * c
