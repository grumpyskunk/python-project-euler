class Euler17:
    """
    If the numbers 1 to 5 are written out in words: one, two, three, four, five...
    then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.

    If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words,
    how many letters would be used?

    NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and forty-two) contains 23 letters
    and 115 (one hundred and fifteen) contains 20 letters.
    The use of "and" when writing out numbers is in compliance with British usage.
    """

    @staticmethod
    def run():
        print(Euler17.__doc__)

        ones = {
            0: '',
            1: 'one',
            2: 'two',
            3: 'three',
            4: 'four',
            5: 'five',
            6: 'six',
            7: 'seven',
            8: 'eight',
            9: 'nine',
        }
        teens = {
            0: 'ten',
            1: 'eleven',
            2: 'twelve',
            3: 'thirteen',
            4: 'fourteen',
            5: 'fifteen',
            6: 'sixteen',
            7: 'seventeen',
            8: 'eighteen',
            9: 'nineteen'
        }
        tens = {
            0: '',
            2: 'twenty',
            3: 'thirty',
            4: 'forty',
            5: 'fifty',
            6: 'sixty',
            7: 'seventy',
            8: 'eighty',
            9: 'ninety'
        }
        hundred = 'hundred'
        thousand = 'thousand'

        blob = ''

        for number in range(1, 1001):
            o = int(number % 10)
            t = int(((number % 100) - o) / 10)
            h = int(((number % 1000) - (t * 10) - o) / 100)

            if number == 1000:
                blob = blob + ones[1] + thousand
            elif h >= 1 and t == 0 and o == 0:
                blob = blob + ones[h] + hundred
            elif h >= 1 and t == 0:
                blob = blob + ones[h] + hundred + 'and' + ones[o]
            elif h >= 1 and t == 1:
                blob = blob + ones[h] + hundred + 'and' + teens[o]
            elif h >= 1:
                blob = blob + ones[h] + hundred + 'and' + tens[t] + ones[o]
            elif t < 1:
                blob = blob + ones[o]
            elif t == 1:
                blob = blob + teens[o]
            elif t > 1:
                blob = blob + tens[t] + ones[o]

        return len(blob)
