from skunk.EulerCommon import EulerCommon


class Euler21:
    """
    Let d(n) be defined as the sum of proper divisors of n (numbers less than n which divide evenly into n).
    If d(a) = b and d(b) = a, where a ≠ b, then a and b are an amicable pair and each of a and b are called
    amicable numbers.

    For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44, 55 and 110; therefore d(220) = 284.
    The proper divisors of 284 are 1, 2, 4, 71 and 142; so d(284) = 220.

    Evaluate the sum of all the amicable numbers under 10000.
    """

    @staticmethod
    def run():
        print(Euler21.__doc__)

        amicable_numbers = set()

        for number in range(2, 10001):
            # Retrieve the factors for our number, then sort and pop them to remove the original number
            n_factors = list(EulerCommon.get_factors(number))
            n_factors.sort()
            n_factors.pop()
            # Evaluate the sum for the remaining factors
            n_sum = EulerCommon.get_list_sum(n_factors)

            # Retrieve the factors for our number sum, then sort and pop them to remove the original number
            s_factors = list(EulerCommon.get_factors(n_sum))
            s_factors.sort()
            s_factors.pop()
            # Evaluate the sum for the remaining factors
            s_sum = EulerCommon.get_list_sum(s_factors)

            # If the current number equals the value of s_sum and the value of n_sum does not equal s_sum
            if number == s_sum and n_sum != s_sum:
                # Add these values to our set
                amicable_numbers.add(number)
                amicable_numbers.add(s_sum)

        return EulerCommon.get_list_sum(list(amicable_numbers))
