class Euler4:
    """
    A palindromic number reads the same both ways.
    The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.

    Find the largest palindrome made from the product of two 3-digit numbers.
    """

    @staticmethod
    def run():
        print(Euler4.__doc__)
        # Initialize our solution as NoneType
        solution = 0
        # Start at the highest possible value
        multiplicand = 999

        for multiplicand in reversed(range(99, 1000)):
            for multiplier in reversed(range(100, 1000)):
                product = multiplicand * multiplier

                # If the product is the same in both directions
                if str(product) == str(product)[::-1]:
                    # If product is greater than the current solution
                    if product > solution:
                        solution = product

                    break

        return solution
