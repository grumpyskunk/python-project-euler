from skunk.EulerCommon import EulerCommon


class Euler14:
    """
    The following iterative sequence is defined for the set of positive integers:

    n → n/2 (n is even)
    n → 3n + 1 (n is odd)

    Using the rule above and starting with 13, we generate the following sequence:
    13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1

    It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms.
    Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.

    Which starting number, under one million, produces the longest chain?

    NOTE: Once the chain starts the terms are allowed to go above one million.
    """

    @staticmethod
    def run():
        print(Euler14.__doc__)

        # Initialize sequence and length dictionaries for mapping
        sequences = dict()
        lengths = dict()

        for current in range(2, 1000000):
            # Initialize a blank sequence and declare our position in the sequence
            sequence = []
            position = current

            while position > 1:
                """
                If this position has previously been calculated, extend our current sequence with
                the stored sequence for this position. Then break for this position sequence
    
                e.g. We only ever evaluate for 6 once, reducing it by 1/2 to 3, then use the
                sequence previously calculated for 3
                3: [3, 10, 5, 16, 8, 4, 2] ... 6: [6, 3, 10, 5, 16, 8, 4, 2]
                """
                if position in sequences.keys():
                    sequence.extend(sequences[position])
                    break

                # Otherwise add our current position to our sequence
                sequence.append(position)

                # If even / else odd
                if EulerCommon.is_divisible(position, 2):
                    position = position // 2
                else:
                    position = (position * 3) + 1

            # Add sequence for this number to our dictionary
            sequences[current] = sequence
            # Add length for this number to our dictionary, keyed by length - it doesn't matter if its overwritten
            lengths[len(sequence)] = current

        # The largest number will have the highest value key
        return lengths[max(lengths.keys())]
